package com.example.greetingproject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends Activity {

    private TextView greetingView;
    private TextView menuView;
    private TextView priceView;

    private Bitmap pictureBitmap;
    private ImageView pictureView;

    private List<Restaurant> restaurants = new ArrayList<Restaurant>();
    private List<RestaurantMenuItem> menus = new ArrayList<RestaurantMenuItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        greetingView = (TextView) findViewById(R.id.helloWorldText);
        serverGreet("/");
        menuView = (TextView) findViewById(R.id.menuView);
        priceView = (TextView) findViewById(R.id.dishPrice);
        serverGreet("/dishes");
        pictureView = (ImageView) findViewById(R.id.dishPicture);
        serverGreet("/friedBeefPicture");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void greetToWorld(View view){
        greetingView = (TextView) findViewById(R.id.helloWorldText);
        greetingView.setText("Hello World!");
    }

    public void serverGreet(String urlMapping){
//        String urlString = "http://example.iana.org/";
        String urlString = "http://10.0.2.2:8080" + urlMapping;

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            new HelloFromServer().execute(urlString);
        } else {
            greetingView.setText("No network connection available.");
        }
    }

    private class HelloFromServer extends AsyncTask<String, Void, String>{
        private static final String DEBUG_TAG = "Main";

        @Override
        protected String doInBackground(String... urls) {
            try{
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve Server Message. URL May be invalid";
            } catch (XmlPullParserException e) {
                return "XML Parsing Failed.";
            }
        }

        @Override
        public void onPostExecute(String result){
            if (restaurants.size() > 0) {
               greetingView.setText(restaurants.get(0).restaurantName);
            }

            if (menus.size() > 0) {
                RestaurantMenuItem menuItem = menus.get(0);
                menuView.setText(menuItem.dishName);
                priceView.setText("UGX " + menuItem.dishPrice);
            }
            pictureView.setImageBitmap(pictureBitmap);
        }

        private String downloadUrl(String url) throws IOException, XmlPullParserException {

            InputStream stream = null;
            int streamLength = 500;

            try{
                URL stringUrl = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) stringUrl.openConnection();
                connection.setReadTimeout(10000 /* milliseconds */);
                connection.setConnectTimeout(15000 /* milliseconds */);
                connection.setRequestMethod("GET");
                connection.setDoInput(true);

                // Starts the query
                connection.connect();
                int response = connection.getResponseCode();
                Log.d(DEBUG_TAG, "The response is: " + response);

                stream = connection.getInputStream();
                if(url.contains("ture")){
                    decodePicture(stream);
                    return "Picture BitMap set";
                }
                return startParse(stream);

            }
            finally {
                if(stream != null){
                    stream.close();
                }
            }
        }

        private void decodePicture(InputStream stream) {
            pictureBitmap = BitmapFactory.decodeStream(stream);
        }

        private String startParse(InputStream stream){
            XmlPullParserFactory pullParserFactory;
            try {
                pullParserFactory = XmlPullParserFactory.newInstance();
                XmlPullParser parser = pullParserFactory.newPullParser();
                InputStream inputStream = stream;
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

                parser.setInput(inputStream, null);

                return parseXML(parser);
            } catch (XmlPullParserException e) {
                return "Pull Parse Exception at startParse()";
            } catch (IOException e) {
                return "IO Exception at startParse()";
            }
        }

        private String parseXML(XmlPullParser parser) throws IOException, XmlPullParserException {
            boolean restaurantExists = false;
            String testName = null;
            int eventType = 0;
            try {
                eventType = parser.getEventType();
            } catch (XmlPullParserException e) {
                testName += "event Type parser error";
            }

            Restaurant currentProduct = null;
            RestaurantMenuItem currentMenu = null;

            testName = "before starting loop";

            while (eventType != XmlPullParser.END_DOCUMENT) {
                testName += " (" + eventType + ") ";

                String name;

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        testName += " " + name + " ";
                        if (name.equals("restaurant")) {
                            restaurantExists = true;
                            currentProduct = new Restaurant();
                        } else if (currentProduct != null) {
                            if (name.equals("name")) {
                                currentProduct.restaurantName = parser.nextText();
                            }
                        }
                        if (name.equals("menu")) {
                            testName += " " + name + " ";
                            testName += "There is a Menu at this Restaurant! \n";
                            currentMenu = new RestaurantMenuItem();
                        } else if (currentMenu != null) {
                            if(name.contains("name")){
                                currentMenu.dishName = parser.nextText();
                            }else if(name.contains("price")){
                                currentMenu.dishPrice = parser.nextText();
                            }
                        }
                        break;
//                    case XmlPullParser.TEXT:
//                        testName += " ["  + parser.getText() + "] ";
//                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("restaurant") && currentProduct != null) {
                            restaurants.add(currentProduct);
                        } else if (name.equalsIgnoreCase("menu") && currentMenu != null) {
                            menus.add(currentMenu);
                        }
                }
                eventType = parser.next();
            }
            return "Done.";
        }
    }
}
